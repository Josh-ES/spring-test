package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    // annotation ensures that HTTP requests to /greeting are mapped to greeting() method
    //
    // - does not specify method, use @RequestMapping(method=GET) to narrow mapping
    //
    // @RequestParam binds value of query string parameter name into name parameter of greeting()
    // method
    //
    // - defaultValue used if absent in request (should required=false be set?)
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {

        // formats string using the template in the controller (String.format method used for this)
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));

    }

}
